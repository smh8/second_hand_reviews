package edu.uscb.csci470sp22;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This class is for searching in the database a query made by a user on
 * index.jsp and populating it with the results (past made reviews).
 * We might should split this class up later but for now going to see if
 * we can fit it all in one class.
 * 
 * @author smh8
 *
 */

// none of this works yet, change if you can make it work
public class SearchAndPopulate extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, 
		HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(false);
		String url = "/index.jsp";
		
		// get current action
		String action = request.getParameter("action");
		if (action == null) {
			action = "startover"; // default action
		} // end if
		
		if (action.equals("startover")) {
	    	url = "/index.jsp";
		} else if (action.equals("post")) {
			String postStr = request.getParameter("postStr");
			String reviewStr = request.getParameter("reviewStr");
		}
		
		getServletContext()
        	.getRequestDispatcher(url)
        	.forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}

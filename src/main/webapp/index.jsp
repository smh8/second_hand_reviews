<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Second Hand Reviews</title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
</head>
<body>
	<header>
		<div><b>
			Second Hand Reviews
			<a href="about.jsp" class="right">ABOUT</a>
			<a href="login.jsp" class="right">LOGIN</a>
		</b></div>
	</header>
	<main>
		<div>Search Here (unimplemented): <br>
			<form action="search" method="post">
				<input type="text" name="searchQuery">
				<input type="submit" value="Search" name="doStuff">
			</form>
			<!-- Placeholder for review population -->
			${results} test
		</div>
		
	</main>
</body>
</html>
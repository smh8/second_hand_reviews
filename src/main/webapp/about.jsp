<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Second Hand Reviews - About Page</title>
<link rel="stylesheet" type="text/css" href="styles/main.css">
</head>
<body>
	<header>
		<b>
			<a href="index.jsp">RETURN</a>
		</b>
	</header>
	<main>
		<p>
			Second Hand Reviews is a reviewing website created by:
			<br>
			Rashaad Griffin, Samuel Hill, and Roosevelt Howard Jr.<br> This website was made
			to create an alternative to posting a review and dealing with trolls.<br> You can
			rest assure knowing that no one can troll here. That's rule #1: No trolls!<br> Second Hand Reviews
			is a way to leave reviews to a product you may have ordered on Amazon, SHEIN, Pandora, or 
			wherever your heart desires.<br> Everyone is one their phones nowadays anyway right? Make use
			of your time by letting people who have similiar interest or even disinterest know about
			products you recently purchased.<br> You also may encounter someone who leaves a review of something
			you're thinking about purchasing.<br> It's a win-win! We appreciate you taking time out to leave a review!
			
		</p>
	</main>

</body>
</html>
package edu.uscb.csci470sp22;

import java.util.Scanner;

public class SearchAndPopulateTest {

	public class Post {

		public String getPostHistoryForUser() {
			Scanner scan = new Scanner(System.in);
			System.out.println("What specific areas in reviews are you looking for today?: ");
			String str = scan.nextLine();
			return str;
		} // end getPostHistoryForUser

	} // end class Post

} // end class SearchAndPopulateTest
